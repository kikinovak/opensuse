TODO
----

* [ ] Exfat-related utilities

* [ ] Recoll or similar desktop search application

* [ ] Documentation: docs.microlinux.fr

* [X] Detailed README

* [X] Upgrade script Leap => Tumbleweed

* [X] Test if list of orphaned packages is empty

* [X] Test upgrades from Leap 15.4

* [X] Test upgrades from Leap 15.3

* [X] Test upgrades from Leap 15.2

* [X] Mailing list for update announcements

* [X] DVD/RW-related applications

* [X] Install custom KDE profile

* [X] Apply custom KDE profile

* [X] Install Microsoft & Eurostile fonts

* [X] Setup everything in one single command

* [ ] Vagrant / VirtualBox

* [ ] NVidia graphics driver

* [ ] HPLIP printer/scanner

* [X] Anydesk

* [ ] OmegaT

* [ ] Citrix Receiver

* [ ] Skype

* [ ] Teams

