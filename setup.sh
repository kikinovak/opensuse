#!/bin/bash
#
# setup.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>
#
# This script turns a standard OpenSUSE Tumbleweed KDE installation into a
# full-blown Linux desktop with bells and whistles. 

# Current directory
CWD=$(pwd)

# Slow things down a bit
SLEEP=1

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then

  echo
  echo "  Please run with sudo or as root." >&2
  echo
  sleep ${SLEEP}

  exit 1

fi

# Make sure we're running OpenSUSE Tumbleweed
source /etc/os-release

if [ "${?}" -ne 0 ]
then

  echo 
  echo "  Unsupported operating system." >&2
  echo
  sleep ${SLEEP}

  exit 1

elif [ "${PRETTY_NAME}" != "openSUSE Tumbleweed" ] 
then

  echo 
  echo "  Please run this script on OpenSUSE Tumbleweed only." >&2
  echo
  sleep ${SLEEP}

  exit 1

fi

echo 
echo "  #########################################"
echo "  # OpenSUSE Tumbleweed KDE configuration #" 
echo "  #########################################"
echo
sleep ${SLEEP}

# Defined users
USERS="$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)"

# Remove these packages
CRUFT=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkgs/useless.txt)

# Additional packages
EXTRA=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkgs/extra.txt)

# DVD/RW-related packages
DVDRW=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkgs/dvdrw.txt)

# Logs
LOG="/var/log/$(basename "${0}" .sh).log"
echo > ${LOG}

# Backup configuration
BACKUPS="cherrytree \
         chromium \
         filezilla \
         keepassxc \
         Microsoft \
         ownCloud \
         soffice.binrc \
         teams \
         transmission \
         VirtualBox \
         VirtualBox6rc \
         VirtualBoxVMrc \
         vlc \
         vlcrc"

# Download mirrors
FLATHUB="https://dl.flathub.org/repo/flathub.flatpakrepo"
MICROLINUX="https://www.microlinux.fr/download"

# Package repositories
REPOS="oss non-oss updates kde packman nvidia dvdcss anydesk"

usage() {

  # Display help message
  echo "  Usage: ${0} OPTION"
  echo
  echo "  OpenSUSE Tumbleweed KDE post-install configuration."
  echo
  echo "  Options:"
  echo "    --shell    Configure Bash, Vim and Xterm."
  echo "    --repos    Setup official and third-party repositories."
  echo "    --fresh    Sync repositories and fetch updates."
  echo "    --strip    Remove unneeded applications."
  echo "    --extra    Install extra tools and applications."
  echo "    --fonts    Install Microsoft and Eurostile fonts."
  echo "    --menus    Configure custom menu entries."
  echo "    --kderc    Install custom KDE profile."
  echo "    --users    Apply custom KDE profile for existing users."
  echo "    --setup    Perform all of the above in one go."
  echo "    --dvdrw    Install DVD/RW-related applications."
  echo
  echo "  Logs are written to ${LOG}."
  echo
  sleep ${SLEEP}

}

configure_shell() {

  echo "  === Shell configuration ==="
  echo
  sleep ${SLEEP}

  echo "  Configuring Bash shell for user: root"
  cp -vf ${CWD}/bash/root-bashrc /root/.bashrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Bash shell for future users."
  cp -vf ${CWD}/bash/user-alias /etc/skel/.alias >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Vim editor for user: root"
  cp -vf ${CWD}/vim/vimrc /root/.vimrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Vim editor for future users."
  cp -vf ${CWD}/vim/vimrc /etc/skel/.vimrc >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Xterm for user: root"
  cp -vf ${CWD}/xterm/Xresources /root/.Xresources >> ${LOG}
  sleep ${SLEEP}

  echo "  Configuring Xterm for future users."
  cp -vf ${CWD}/xterm/Xresources /etc/skel/.Xresources >> ${LOG}
  sleep ${SLEEP}

  if [ ! -z "${USERS}" ]
  then

    for USER in ${USERS}
    do

      if [ -d /home/${USER} ]
      then

        echo "  Configuring Bash shell for user: ${USER}"
        cp -vf ${CWD}/bash/user-alias /home/${USER}/.alias >> ${LOG}
        chown -v ${USER}:users /home/${USER}/.alias >> ${LOG}
        sleep ${SLEEP}

        echo "  Configuring Vim editor for user: ${USER}"
        cp -vf ${CWD}/vim/vimrc /home/${USER}/.vimrc >> ${LOG}
        chown -v ${USER}:users /home/${USER}/.vimrc >> ${LOG}
        sleep ${SLEEP}

        echo "  Configuring Xterm for user: ${USER}"
        cp -vf ${CWD}/xterm/Xresources /home/${USER}/.Xresources >> ${LOG}
        chown -v ${USER}:users /home/${USER}/.Xresources >> ${LOG}
        sleep ${SLEEP}

      fi

    done

  fi

  echo "  Configuring SSH server."
  cp -vf ${CWD}/sshd/custom.conf /etc/ssh/sshd_config.d/ >> ${LOG}
  systemctl reload sshd
  sleep ${SLEEP}

echo

}

configure_repos() {

  echo "  === Package repository configuration ==="
  echo
  sleep ${SLEEP}

  echo "  Removing existing repositories."
  rm -f /etc/zypp/repos.d/*.repo*
  sleep ${SLEEP}

  for REPO in ${REPOS}
  do

    echo "  Configuring repository: ${REPO}"
    cp -v ${CWD}/repos.d/${REPO}.repo /etc/zypp/repos.d/ >> ${LOG} 2>&1
    sleep ${SLEEP}

  done

  echo

}

update_system() {

  echo "  === Update system ==="
  echo
  sleep ${SLEEP}

  echo "  Refreshing repository information."
  sleep ${SLEEP}
  echo "  This might take a moment..."
  zypper --gpg-auto-import-keys refresh >> ${LOG} 2>&1

  if [ "${?}" -ne 0 ]
  then

    echo "  Could not refresh repository information." >&2
    exit 1

  fi

  echo "  Updating system with enhanced packages."
  sleep ${SLEEP}
  echo "  This might also take a moment..."
  zypper --non-interactive dist-upgrade --allow-vendor-change \
    --auto-agree-with-licenses --no-recommends --replacefiles >> ${LOG} 2>&1

  if [ "${?}" -ne 0 ]
  then

    echo "  Could not perform system update." >&2
    echo
    exit 1

  fi

  echo

}

strip_system() {

  echo "  === Remove useless packages ==="
  echo
  sleep ${SLEEP}

  for PACKAGE in ${CRUFT}
  do

    if rpm -q ${PACKAGE} > /dev/null 2>&1 
    then

      echo "  Removing package: ${PACKAGE}"
      zypper --non-interactive remove --clean-deps ${PACKAGE} >> ${LOG} 2>&1

      if [ "${?}" -ne 0 ]
        then

        echo "  Could not remove package ${PACKAGE}." >&2
        echo
        exit 1

      fi

    fi

  done

  echo "  All useless packages removed from the system."
  echo
  sleep ${SLEEP}

}

install_extra() {

  echo "  === Install extra packages ==="
  echo
  sleep ${SLEEP}

  for PACKAGE in ${EXTRA}
  do

    if ! rpm -q ${PACKAGE} > /dev/null 2>&1 
    then

      echo "  Installing package: ${PACKAGE}"
      zypper --non-interactive install --no-recommends \
        --allow-vendor-change ${PACKAGE} >> ${LOG} 2>&1

      if [ "${?}" -ne 0 ]
        then

        echo "  Could not install package ${PACKAGE}." >&2
        echo
        exit 1

      fi

    fi

  done

  echo "  Configuring repository: flathub"
  flatpak remote-add --if-not-exists flathub ${FLATHUB}

  echo "  All extra packages installed on the system."
  echo 
  sleep ${SLEEP}

}

install_dvdrw() {

  echo "  === Install DVD/RW-related applications ==="
  echo
  sleep ${SLEEP}

  for PACKAGE in ${DVDRW}
  do

    if ! rpm -q ${PACKAGE} > /dev/null 2>&1 
    then

      echo "  Installing package: ${PACKAGE}"
      zypper --non-interactive install --no-recommends \
        --allow-vendor-change ${PACKAGE} >> ${LOG} 2>&1

      if [ "${?}" -ne 0 ]
        then

        echo "  Could not install package ${PACKAGE}." >&2
        echo
        exit 1

      fi

    fi

  done

  echo "  All DVD/RW-related packages installed on the system."
  echo 
  sleep ${SLEEP}

}

install_fonts() {

  echo "  === Install additional TrueType fonts ==="
  echo
  sleep ${SLEEP}

  # Microsoft fonts
  if [ ! -d /usr/share/fonts/truetype/microsoft ]
  then

    pushd /tmp >> ${LOG} 2>&1
    rm -rf /usr/share/fonts/truetype/microsoft
    rm -rf /usr/share/fonts/truetype/msttcorefonts

    echo "  Installing Microsoft TrueType fonts."
    wget -c --no-check-certificate \
      ${MICROLINUX}/webcore-fonts-3.0.tar.gz >> ${LOG} 2>&1 \
    wget -c --no-check-certificate \
      ${MICROLINUX}/symbol.gz >> ${LOG} 2>&1

    mkdir /usr/share/fonts/truetype/microsoft
    tar xvf webcore-fonts-3.0.tar.gz >> ${LOG} 2>&1

    pushd webcore-fonts >> ${LOG} 2>&1

    if type fontforge > /dev/null 2>&1
    then

      fontforge -lang=ff -c 'Open("vista/CAMBRIA.TTC(Cambria)"); \
        Generate("vista/CAMBRIA.TTF");Close();Open("vista/CAMBRIA.TTC(Cambria Math)"); \
        Generate("vista/CAMBRIA-MATH.TTF");Close();' >> ${LOG} 2>&1
      rm vista/CAMBRIA.TTC

    fi

    cp fonts/* /usr/share/fonts/truetype/microsoft/
    cp vista/* /usr/share/fonts/truetype/microsoft/

    popd >> ${LOG} 2>&1

    fc-cache -f -v >> ${LOG} 2>&1

  fi

  # Eurostile fonts
  if [ ! -d /usr/share/fonts/eurostile ]
  then

    cd /tmp
    rm -rf /usr/share/fonts/eurostile

    echo "  Installing Eurostile TrueType fonts."
    wget -c --no-check-certificate ${MICROLINUX}/Eurostile.zip >> ${LOG} 2>&1

    unzip Eurostile.zip -d /usr/share/fonts/ >> ${LOG} 2>&1
    mv /usr/share/fonts/Eurostile /usr/share/fonts/eurostile

    fc-cache -f -v >> ${LOG} 2>&1

    rm -f Eurostile.zip

    cd - >> ${LOG} 2>&1

  fi

  echo "  Additional TrueType fonts installed on the system."
  echo
  sleep ${SLEEP}

}

rewrite_menus() {

  ENTRIESDIR="${CWD}/menus"
  ENTRIES=$(ls ${ENTRIESDIR})
  MENUDIRS="/usr/share/applications \
            /var/lib/flatpak/exports/share/applications" 

  echo "  === Install custom desktop menu ==="
  echo
  sleep ${SLEEP}

  for MENUDIR in ${MENUDIRS}
  do

    for ENTRY in ${ENTRIES}
    do

      if [ -r ${MENUDIR}/${ENTRY} ]
      then

        echo "  Rewriting menu item: ${ENTRY}"
        sed -i '/^#/d' ${MENUDIR}/${ENTRY}
        sed -i '/^\[Desktop Action/Q' ${MENUDIR}/${ENTRY}
        sed -i '/^\[X-Drawing/Q' ${MENUDIR}/${ENTRY}
        sed -i '/^\[X-Property/Q' ${MENUDIR}/${ENTRY}
        sed -i '/^GenericName/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Name/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Comment/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Keywords/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Actions/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Version/d' ${MENUDIR}/${ENTRY}
        sed -i '/^Categories/d' ${MENUDIR}/${ENTRY}
        sed -i '/^NoDisplay/d' ${MENUDIR}/${ENTRY}
        sed -i '/^$/d' ${MENUDIR}/${ENTRY}
        cat ${ENTRIESDIR}/${ENTRY} >> ${MENUDIR}/${ENTRY}
        sleep 0.1

      fi

    done

  done

  echo "  Updating desktop database."
  update-desktop-database
  sleep ${SLEEP}

  echo "  Custom desktop menu installed."
  sleep ${SLEEP}

  echo

}

install_profile() {

  PLASMA="/usr/share/plasma/plasmoids"
  TASKBAR="${PLASMA}/org.kde.plasma.taskmanager/contents/config/main.xml"
  KICKOFF="${PLASMA}/org.kde.plasma.kickoff/contents/config/main.xml"

  echo "  === Configure KDE desktop environment ==="
  echo 
  sleep ${SLEEP}

  echo "  Configuring taskbar."
  sed -i -e 's/applications:systemsettings.desktop/applications:org.kde.dolphin.desktop/g' ${TASKBAR}
  sed -i -e 's/applications:org.kde.discover.desktop/applications:firefox.desktop/g' ${TASKBAR}
  sed -i -e 's/preferred:\/\/filemanager/applications:thunderbird.desktop/g' ${TASKBAR}
  sed -i -e 's/preferred:\/\/browser/applications:libreoffice-startcenter.desktop/g' ${TASKBAR}
  sleep ${SLEEP}

  echo "  Configuring Kickoff menu."
  sed -i -e 's/preferred:\/\/browser/firefox.desktop/g' ${KICKOFF}
  sed -i -e 's/org.kde.kontact.desktop/thunderbird.desktop/g' ${KICKOFF}
  sed -i -e 's/writer.desktop,libreoffice-writer.desktop/libreoffice-startcenter.desktop/g' ${KICKOFF}
  sed -i -e 's/org.kde.digikam.desktop,//g' ${KICKOFF}
  sed -i -e 's/org.kde.kate.desktop,//g' ${KICKOFF}
  sed -i -e 's/systemsettings.desktop,//g' ${KICKOFF}
  sed -i -e 's/org.kde.Help.desktop,//g' ${KICKOFF}
  sed -i -e 's/org.kde.khelpcenter.desktop,//g' ${KICKOFF}
  sed -i -e 's/org.kde.konsole.desktop//g' ${KICKOFF}
  sed -i -e 's/org.kde.dolphin.desktop,/org.kde.dolphin.desktop/g' ${KICKOFF}
  sed -i -e 's/suspend,hibernate/logout/g' ${KICKOFF}
  sleep ${SLEEP}

  echo "  Removing existing profile."
  rm -rf /etc/skel/.config
  mkdir /etc/skel/.config
  sleep ${SLEEP}

  echo "  Installing custom KDE profile."
  cp -v ${CWD}/kde/* /etc/skel/.config/ >> ${LOG} 2>&1
  sleep ${SLEEP}

  echo

}

apply_profile() {

  echo "  === Apply custom KDE configuration ==="
  echo
  sleep ${SLEEP}

  if [ ! -d /etc/skel/.config ]
  then

    echo "  Custom profiles are not installed." >&2
    echo
    exit 1

  fi

  for USER in ${USERS}
  do

    if [ -d /home/${USER} ] 
    then

      echo "  Updating profile for user: ${USER}"
      rm -rf /home/${USER}/.config.bak
      mv -f /home/${USER}/.config /home/${USER}/.config.bak
      cp -R /etc/skel/.config /home/${USER}/

      for BACKUP in ${BACKUPS}
      do

        if [ -r /home/${USER}/.config.bak/${BACKUP} ]
        then 

          cp -R /home/${USER}/.config.bak/${BACKUP} /home/${USER}/.config/

        fi

      done

      chown -R ${USER}:users /home/${USER}/.config
      sleep ${SLEEP}

    fi

  done

  echo

}

# Check parameters.
if [[ "${#}" -ne 1 ]]
then

  usage
  exit 1

fi

OPTION="${1}"

case "${OPTION}" in

  --shell) 
    configure_shell
    ;;

  --repos)
    configure_repos
    ;;

  --fresh)
    update_system
    rewrite_menus
    install_profile
    ;;

  --strip) 
    strip_system
    ;;

  --extra)
    install_extra
    ;;

  --dvdrw)
    install_dvdrw
    ;;

  --fonts) 
    install_fonts
    ;;

  --menus) 
    rewrite_menus
    ;;

  --kderc) 
    install_profile
    ;;

  --users) 
    apply_profile
    ;;

  --setup)
    configure_shell
    configure_repos
    update_system
    strip_system
    install_extra
    install_fonts
    rewrite_menus
    install_profile
    apply_profile
    ;;

  --help) 
    usage
    exit 0
    ;;

  ?*) 
    usage
    exit 1

esac

exit 0
